<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Task;
use Illuminate\Http\Response;

class ProjectTaskAssignationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Project $project
     * @param Task $task
     * @return Response
     */
    public function store(Project $project, Task $task)
    {
        $project->tasks()
            ->attach($task);

        return response()
            ->json("Task has been assigned to $project->name", 200);
    }
}
