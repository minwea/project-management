<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TaskMemberAssignationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Task $task
     * @param User $member
     * @return Response
     */
    public function store(Task $task, User $member)
    {
        $task->members()
            ->attach($member);

        return response()
            ->json(['message' => "Task has been assigned to $member->name"], 200);
    }
}
