<?php

namespace App\Http\Controllers;

use App\Models\JobTemplate;
use App\Models\Project;
use App\Models\Task;
use Illuminate\Http\Response;

class CompleteTaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Task $task
     * @return Response
     */
    public function store(Task $task)
    {
        $task->complete()->create([
            'user_id' => auth()->id()
        ]);

        //then complete task observer will automatically update user information

        return response()->json(['message' => "Congratulations, you have completed your task."], 200);
    }
}
