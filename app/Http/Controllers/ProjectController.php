<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Response;

class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $projects = Project::latest()
            ->get();

        return response()
            ->json($projects, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $data = request()->validate([
            'name' => 'required|string',
            'description' => 'required|string'
        ]);

        $project = Project::create($data);

        return response()->json([
            'message' => 'Project has been created.',
            'data' => $project,
        ], 200);
    }
}
