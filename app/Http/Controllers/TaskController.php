<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Response;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $tasks = Task::latest()
            ->get();

        return response()->json([
            'data' => $tasks,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $data = request()->validate([
            'title' => 'required|string',
            'description' => 'required|string',
            'deadline' => 'required|date',
            'status' => 'required|boolean',
        ]);

        $task = Task::create($data);

        return response()->json([
            'message' => 'New task has been created.',
            'data' => $task,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Task $task
     * @return void
     */
    public function update(Task $task)
    {
        $data = request()->validate([
            'title' => 'required|string',
            'description' => 'required|string',
            'deadline' => 'required|date',
            'status' => 'required|boolean',
        ]);

        $task = $task->update($data);

        return response()->json([
            'message' => 'Task has been updated.',
            'data' => $task,
        ], 200);
    }
}
