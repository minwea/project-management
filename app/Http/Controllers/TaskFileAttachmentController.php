<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TaskFileAttachmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param Task $task
     * @return Response
     */
    public function store(Request $request, Task $task)
    {
        request()->validate([
            'title' => 'required|string',
            'description' => 'required|string',
            'file' => 'required|mimes:pdf|max:5000',
        ]);

        $file = $request->file('file');
        $url = $file->store('task');

        $task->files()->create([
            'title' => request('title'),
            'description' => request('description'),
            'url' => $url,
        ]);

        return response()
            ->json(['message' => 'File has been uploaded.'], 200);
    }
}
