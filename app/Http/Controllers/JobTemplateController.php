<?php

namespace App\Http\Controllers;

use App\Models\JobTemplate;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class JobTemplateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Project $project
     * @return Response
     */
    public function store(Project $project)
    {
        $data = request()->validate([
            'name' => 'required|string'
        ]);

        $project->templates()->create($data);

        return response()->json(['message' => 'Template has been created.'], 200);
    }
}
