<?php

namespace App\Http\Controllers;

use App\Models\JobTemplate;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class JobTemplateProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param JobTemplate $template
     * @return Response
     */
    public function store(JobTemplate $template)
    {
        $projectTemplate = $template->project;

        $project = Project::create([
            'name' => $projectTemplate->name,
            'description' => $projectTemplate->description,
        ]);


        /**
         * During creation of project or tasks, it shouldn't save into db directly. It should display the template on frontend and let user review before save.
         * Since now its skeleton code, i'll assume user already reviewed
         *
         * $tasks = INSERT INTO tasks(
            SELECT title, description, deeadline, STATUS
            FROM
                tasks
            WHERE
                id IN (SELECT task_id
                FROM
                    project_tasks
                WHERE
                    project_id = $template->project_id
                )
            )
         *  $tasks->project()->attach($project);
         */

        return response()->json(['message' => "Project has been created based on template $template->name."], 200);
    }
}
