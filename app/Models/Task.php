<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $guarded = [];

    use HasFactory;

    public function members()
    {
        return $this->belongsToMany(User::class, 'user_tasks');
    }

    public function files()
    {
        return $this->morphToMany(File::class, 'fileable');
    }

    public function complete()
    {
        return $this->hasMany(CompleteTask::class);
    }

    public function project()
    {
        return $this->belongsToMany(Project::class);
    }
}
