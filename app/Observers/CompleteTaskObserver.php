<?php

namespace App\Observers;

use App\Models\CompleteTask;

class CompleteTaskObserver
{
    /**
     * Handle the complete task "created" event.
     *
     * @param CompleteTask $completeTask
     * @return void
     */
    public function created(CompleteTask $completeTask)
    {
        //update user information
    }
}
