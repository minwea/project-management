<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'project'], function() {
    Route::get('', 'ProjectController@index');
    Route::post('', 'ProjectController@store');
    Route::post('{project}/task/{task}', 'ProjectTaskAssignationController@store');
    Route::post('{project}/template', 'JobTemplateController@store');
});

Route::post('job-template/{template}/project', 'JobTemplateProjectController@store');
Route::post('job-template/{project}', 'JobTemplateController@store');

Route::group(['prefix' => 'task'], function() {
    Route::get('', 'TaskController@index');
    Route::post('', 'TaskController@store');
    Route::post('{task}/file', 'TaskFileAttachmentController@store');
    Route::post('{task}/complete', 'CompleteTaskController@store');
    Route::post('{task}/member/{member}', 'TaskMemberAssignationController@store');
});
